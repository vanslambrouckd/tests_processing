float radius;

void setup() {
  smooth();
  frameRate(60);
  colorMode(HSB, 360, 100, 100);
  size(800,800);
  radius = 200;
}


void draw() {
  background(360);
  translate(width/2, height/2);
  stroke(0);
  strokeWeight(2);
    
  for (float angle = 0; angle < 360; angle+= 0.1) {
    float theta = radians(angle);
    float x = cos(theta)*radius;
    float y = sin(theta)*radius;
    
    strokeWeight(2);
    stroke(0, 0, 0);
    point(x, y);
  }
  
  strokeWeight(2);
  stroke(0, 0, 0);
  line(-radius, 0, radius, 0);
  line(0, -radius, 0, radius);
    
  float mX = map(mouseX, 0, width, -width/2, width/2);
  float mY = map(mouseY, 0, height, -height/2, height/2);
  
  line(0, 0, mX, mY);
  
  strokeWeight(1);
  
  PVector v1 = new PVector(radius, 0);
  PVector v2 = new PVector(mX, mY);
  
  float a = PVector.angleBetween(v2, v1);
  
  if (v2.y > 0) {
    a = TWO_PI - a;
  }
  
  strokeWeight(5);
  stroke(200, 100, 100);  
  line(0, 0, v1.x, v1.y);
  stroke(360, 100, 100); 
  line(0, 0, v2.x, v2.y);
  noFill();
  arc(0, 0, radius, radius, -a, 0, OPEN);
  fill(0);
  textSize(30);
  float deg = degrees(a);
  text((int)deg + "°", v2.x+20, v2.y+20);
  textSize(12);
  text(a + " rad", v2.x+2, v2.y+40);
  
  fill(360);
  
  //drawCos();
}

/*
void drawCos() {
  stroke(100, 100, 100);
  strokeWeight(4);
  
  translate(0, 0);
  float mX = mouseX; //map(mouseX, 400, 600, 0 ,600);
  println("mouseX", mouseX);
  //float x = map(mouseX, 0, radius, 0, radius);
  
  float tmp = (width/2)-mouseX;
  tmp *= -1;
  
  println("tmp", tmp);
  println("-radius", -radius);
  if (mouseX > radius && mouseX < (width/2+radius)) {
    line(0, 0, tmp, 0);
  } else {
    println("kleiner");
  }
  
  
}

void drawSin() {
  float mX = map(mouseX, 0, width, -width/2, width/2);
  float x = map(mX, 0, radius, width/2, (width/2)+radius);
  
  line(0, 0, 0, x);
}
*/