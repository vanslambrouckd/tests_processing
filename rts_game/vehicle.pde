class Vehicle {
  PVector loc;
  PVector velocity;
  PVector acceleration;
  float maxspeed;
  float w;
  float h;
  color col;
  float health;
  
  Vehicle(float x, float y, color c) {
    health = 100;
    
    this.acceleration = new PVector(0,0);
    this.velocity = new PVector(0, 0);
    this.loc = new PVector(x, y);
    this.w = this.h = 20;
    this.col = c;
  }
  
  void draw() {
    fill(this.col);
    rect(this.loc.x, this.loc.y, this.w, this.h);
  }
  
  void seek(PVector target) {
    PVector desired = PVector.sub(target, this.loc);
    desired.normalize();
    desired.mult(maxspeed);
    
    PVector steer = PVector.sub(desired, velocity);
    applyForce(steer);
  }
  
  void applyForce(PVector force) {
    acceleration.add(force);
  }
  
  void update() {
    velocity.add(acceleration);
    loc.add(velocity);
    
    acceleration.mult(0);
  }
}